#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

DIR=tablelayout-$2
TAR=../libtablelayout-java_$2.orig.tar.gz

# clean up the upstream tarball
mkdir -p $DIR/src/org/debian/tablelayout
(cd $DIR && jar xf ../$3)
mv $DIR/info/clearthought/layout/* $DIR/src/org/debian/tablelayout/
rm -rf $DIR/info/
tar -c -z -f $TAR $DIR
rm -rf $DIR
rm -rf $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

